package uk.co.gencoreoperative.ocelot;

import com.google.inject.*;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import uk.co.gencoreoperative.ocelot.cli.*;
import uk.co.gencoreoperative.ocelot.config.ConfigManager;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;
import uk.co.gencoreoperative.ocelot.tasks.Coordinator;
import uk.co.gencoreoperative.ocelot.utils.Stats;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Responsible for starting the application.
 *
 * @author Robert Wapshott
 */
public class Ocelot {
    private static Injector injector;

    public static void main(String[] args) {
        Set<Mode> modes = new HashSet<Mode>();
        modes.add(getInstance(Bounce.class));
        modes.add(getInstance(Deploy.class));
        modes.add(getInstance(DeployThis.class));
        modes.add(getInstance(Help.class));
        modes.add(getInstance(Init.class));
        modes.add(getInstance(Run.class));
        modes.add(getInstance(Show.class));
        modes.add(getInstance(War.class));

        JSAP parser = getInstance(JSAP.class);
        try {
            for (Mode mode : modes) {
                parser.registerParameter(mode.getParameter());
            }
        } catch (JSAPException e) {
            throw new IllegalStateException(e);
        }

        JSAPResult result = parser.parse(args);
        if (!result.success()) {
            getInstance(Help.class).perform(result);
            log("");
            exit("Invalid arguments provided.");
        }

        // Check which mode was provided, only perform the first.
        boolean performed = false;
        for (Mode mode : modes) {
            if (mode.canPerform(result)) {
                mode.perform(result);
                performed = true;
                break;
            }
        }

        // Default is Run mode
        if (!performed) {
            getInstance(Run.class).perform(result);
        }
    }

    public synchronized static <T> T getInstance(Class<T> clazz) {
        if (injector == null) {
            injector = Guice.createInjector(new Module());
        }
        return injector.getInstance(clazz);
    }

    public static void log(String msg) {
        System.out.println(msg);
    }

    public static void exit(String msg) {
        log(msg);
        System.exit(0);
    }

    private static class Module extends AbstractModule {
        @Override
        protected void configure() {
            bind(ExecutorService.class).toInstance(Executors.newFixedThreadPool(8));
            bind(OcelotConfig.class).toProvider(new Provider<OcelotConfig>() {
                @Override
                public OcelotConfig get() {
                    return getInstance(ConfigManager.class).load();
                }
            });
            bind(Coordinator.class).in(Singleton.class);
            bind(Stats.class).in(Singleton.class);
            bind(JSAP.class).in(Singleton.class);
        }
    }
}
