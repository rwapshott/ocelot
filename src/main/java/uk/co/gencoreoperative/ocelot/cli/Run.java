package uk.co.gencoreoperative.ocelot.cli;

import com.google.inject.Inject;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.OcelotConstants;
import uk.co.gencoreoperative.ocelot.tasks.Coordinator;
import uk.co.gencoreoperative.ocelot.tasks.FolderScanner;
import uk.co.gencoreoperative.ocelot.utils.Stats;

import java.text.MessageFormat;
import java.util.concurrent.ExecutorService;

/**
 * Responsible for performing the a file comparison and when complete, restarting Tomcat.
 *
 * @author Robert Wapshott
 */
public class Run implements Mode {
    // Injected
    private final ExecutorService service;
    private final Stats stats;
    private final Coordinator coordinator;
    private final FolderScanner scanner;
    private final Bounce bounceMode;

    @Inject
    public Run(ExecutorService service, Stats stats, Coordinator coordinator, FolderScanner scanner, Bounce bounceMode) {
        this.service = service;
        this.stats = stats;
        this.coordinator = coordinator;
        this.scanner = scanner;
        this.bounceMode = bounceMode;
    }

    @Override
    public Parameter getParameter() {
        return new Switch(OcelotConstants.RUN_MODE)
                .setShortFlag('r')
                .setLongFlag(OcelotConstants.RUN_MODE)
                .setHelp("The default operation, search for files to copy and once copied, restart Tomcat.");
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getBoolean(getParameter().getID());
    }

    @Override
    public void perform(JSAPResult result) {
        long start = System.currentTimeMillis();

        // Signal we are starting scanning.
        coordinator.setScanning(true);
        service.execute(scanner);

        while (coordinator.isScanning() || coordinator.hasFilesToProcess()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        // Completion detected.
        service.shutdown();
        long delta = System.currentTimeMillis() - start;


        if (stats.getFilesCopied() > 0) {
            // Handle Tomcat restart.
            bounceMode.perform(result);

            // Report to the user.
            Ocelot.log("");
            Ocelot.log(stats.toString());
            Ocelot.log(MessageFormat.format("Scan took {0}ms", delta));
        } else {
            Ocelot.log("No files changed.");
        }
    }
}
