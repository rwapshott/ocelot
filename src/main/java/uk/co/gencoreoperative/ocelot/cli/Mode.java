package uk.co.gencoreoperative.ocelot.cli;

import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;

/**
 * Mode interface defines the functions required for command line processing.
 *
 * We need to determine which mode of operation the user has specified, and then
 * perform that mode.
 *
 * @author Robert Wapshott
 */
public interface Mode {
    Parameter getParameter();

    /**
     * Indicates if the Mode applies to the results of the parsed command line arguments.
     * @param result Non null results.
     * @return True if this Mode applies, otherwise false.
     */
    boolean canPerform(JSAPResult result);

    /**
     * Perform the operation.
     * @param result Results from the parsing of the arguments.
     */
    void perform(JSAPResult result);
}
