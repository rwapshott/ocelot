package uk.co.gencoreoperative.ocelot.cli;

import com.google.inject.Inject;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import uk.co.gencoreoperative.fileutils.SearchUtils;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.OcelotConstants;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;
import uk.co.gencoreoperative.ocelot.utils.FilteredSearchAction;
import uk.co.gencoreoperative.ocelot.utils.Filters;

import java.io.File;

/**
 * Responsible for showing all files that will be examined by the file search.
 *
 * @author Robert Wapshott
 */
public class Show implements Mode {
    // Injected
    private final OcelotConfig config;

    public Parameter getParameter() {
        return new Switch(OcelotConstants.SHOW_MODE)
                .setShortFlag('s')
                .setLongFlag(OcelotConstants.SHOW_MODE)
                .setHelp("Shows all files that will be selected by " + OcelotConstants.APP_NAME);
    }

    @Inject
    public Show(OcelotConfig config) {
        this.config = config;
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getBoolean(getParameter().getID());
    }

    @Override
    public void perform(JSAPResult result) {
        SearchUtils.Action action = new SearchUtils.Action() {
            @Override
            public void perform(File file) {
                Ocelot.log(file.getPath());
            }
        };
        SearchUtils.iterateTopDown(
                config.getDevelopmentFolder(),
                new FilteredSearchAction(config, action, new Filters()));

        Ocelot.log("");
        Ocelot.log("Filters:");
        for (String filter : config.getClassFilters()) {
            Ocelot.log(filter);
        }
    }
}
