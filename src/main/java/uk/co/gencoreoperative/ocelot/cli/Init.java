package uk.co.gencoreoperative.ocelot.cli;

import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.OcelotConstants;
import uk.co.gencoreoperative.ocelot.config.ConfigManager;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Responsible for generating a default configuration which the user can edit as required.
 *
 * @author Robert Wapshott
 */
public class Init implements Mode {

    @Override
    public Parameter getParameter() {
        return new Switch(OcelotConstants.INIT_MODE)
                .setShortFlag('i')
                .setLongFlag(OcelotConstants.INIT_MODE)
                .setHelp("Initialises a default configuration, overwriting any previous.");
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getBoolean(getParameter().getID());
    }

    @Override
    public void perform(JSAPResult result) {
        ConfigManager manager = Ocelot.getInstance(ConfigManager.class);

        OcelotConfig config = new OcelotConfig();
        config.setDevelopmentFolder(new File("/Users/robert/dev/openam"));
        config.setApacheTomcatHome(new File("/Users/robert/dev/apache-tomcat-6.0.36"));
        config.setWebAppName("openam");
        config.setWarName("openam-server-10.2.0-SNAPSHOT.war");

        Set<String> filters = new HashSet<String>();
        filters.add("target*classes*openmam-core*.class");
        config.setClassFilters(filters);

        manager.save(config);

        Ocelot.log("Default configuration has been generated");
    }
}
