package uk.co.gencoreoperative.ocelot.cli;

import com.google.inject.Inject;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import uk.co.gencoreoperative.ocelot.OcelotConstants;
import uk.co.gencoreoperative.ocelot.utils.Tomcat;

/**
 * Responsible for restarting Tomcat.
 *
 * @author Robert Wapshott
 */
public class Bounce implements Mode {
    // Injected
    private final Tomcat tomcat;

    @Inject
    public Bounce(Tomcat tomcat) {
        this.tomcat = tomcat;
    }

    @Override
    public Parameter getParameter() {
        return new Switch(OcelotConstants.BOUNCE_MODE)
                .setShortFlag('b')
                .setLongFlag(OcelotConstants.BOUNCE_MODE)
                .setHelp("Restarts Tomcat with no other changes.");
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getBoolean(getParameter().getID());
    }

    @Override
    public void perform(JSAPResult result) {
        tomcat.kill();
        tomcat.start();
    }
}