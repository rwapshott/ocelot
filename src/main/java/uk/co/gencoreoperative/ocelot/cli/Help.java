package uk.co.gencoreoperative.ocelot.cli;

import com.google.inject.Inject;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import uk.co.gencoreoperative.fileutils.StreamUtils;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.OcelotConstants;

import java.util.List;

/**
 * Responsible for
 *
 * @author robert.wapshott@forgerock.com
 */
public class Help implements Mode {
    // Injected
    private final JSAP parser;

    @Inject
    public Help(JSAP parser) {
        this.parser = parser;
    }

    private String getLastReleaseVersion() {
        List<String> lines = StreamUtils.readLinesToList(Help.class.getResourceAsStream("/RELEASE_NOTES"));
        if (lines.isEmpty()) return null;
        String last = lines.get(lines.size() - 1);
        return last.split("\\s+")[0];
    }

    @Override
    public Parameter getParameter() {
        return new Switch(OcelotConstants.HELP_MODE)
                .setShortFlag('h')
                .setLongFlag(OcelotConstants.HELP_MODE)
                .setHelp("This help text.");
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getBoolean(getParameter().getID());
    }

    @Override
    public void perform(JSAPResult result) {
        Ocelot.log(OcelotConstants.APP_NAME + " " + getLastReleaseVersion());
        Ocelot.log("");
        Ocelot.log("Usage:");
        Ocelot.log(OcelotConstants.APP_NAME + " " + parser.getUsage());
        Ocelot.log("");
        Ocelot.log(parser.getHelp());
    }
}
