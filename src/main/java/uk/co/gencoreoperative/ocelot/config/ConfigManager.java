package uk.co.gencoreoperative.ocelot.config;

import com.google.gson.Gson;
import com.google.inject.Inject;
import uk.co.gencoreoperative.fileutils.StreamUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Responsible for managing the loading and saving of the configuration.
 *
 * @author Robert Wapshott
 */
public class ConfigManager {

    private Gson gson;

    @Inject
    public ConfigManager(Gson gson) {
        this.gson = gson;
    }

    public File getSettings() {
        return new File(System.getProperty("user.home"), ".ocelot_settings");
    }

    public OcelotConfig load() {
        File settings = getSettings();
        if (!settings.exists()) {
            return null;
        }

        try {
            String json = StreamUtils.readLines(new FileInputStream(getSettings()));
            OcelotConfig config = gson.fromJson(json, OcelotConfig.class);
            return config;
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public void save(OcelotConfig config) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(getSettings(), false));

            String json = gson.toJson(config);

            writer.write(json);
            writer.newLine();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
