package uk.co.gencoreoperative.ocelot.utils;

import com.google.inject.Inject;
import uk.co.gencoreoperative.fileutils.SearchUtils;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;

import java.io.File;

/**
 * Responsible for performing the role of a SearchUtils.Action, however, it will
 * only delegate to its given Action if the file path matches one of the the OcelotConfig
 * filters.
 *
 * @author Robert Wapshott
 */
public class FilteredSearchAction implements SearchUtils.Action {
    // Injected
    private final OcelotConfig config;
    private final SearchUtils.Action action;
    private final Filters filters;


    @Inject
    public FilteredSearchAction(OcelotConfig config, SearchUtils.Action action, Filters filters) {
        this.config = config;
        this.action = action;
        this.filters = filters;
    }

    @Override
    public void perform(File file) {
        if (file.isDirectory()) return;
        String path = file.getPath();
        for (String filter : config.getClassFilters()) {
            if (filters.filterMatch(filter, path)) {
                action.perform(file);
                break;
            }
        }
    }


}
