package uk.co.gencoreoperative.ocelot.utils;

/**
 * Responsible for providing functions for filtering.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Filters {
    public boolean filterMatch(String filter, String path) {
        String[] parts = filter.split("\\*");
        for (String part : parts) {
            if (!path.contains(part)) {
                return false;
            }
        }
        return true;
    }
}
