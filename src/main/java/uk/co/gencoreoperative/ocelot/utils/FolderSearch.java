package uk.co.gencoreoperative.ocelot.utils;

import uk.co.gencoreoperative.fileutils.SearchUtils;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;

import javax.inject.Inject;
import java.io.File;
import java.util.Collection;

/**
 * Responsible for providing utilities for searching for files.
 *
 * @author robert.wapshott@forgerock.com
 */
public class FolderSearch {
    private final OcelotConfig config;

    @Inject
    public FolderSearch(OcelotConfig config) {
        this.config = config;
    }

    /**
     * Searches the given folder for the war file defined in configuration.
     *
     * @see uk.co.gencoreoperative.ocelot.config.OcelotConfig#getWarName()
     * @param folder Folder to search for the war file.
     * @return Null if not found, otherwise the matched File.
     */
    public File findWar(File folder) {
        Collection<File> files = SearchUtils.search(
                folder,
                new SearchUtils.Search() {
                    @Override
                    public boolean include(File file) {
                        return file.getName().equals(config.getWarName());
                    }
                });

        if (files.size() == 1) {
            return files.iterator().next();
        }

        return null;
    }
}
