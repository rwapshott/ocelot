package uk.co.gencoreoperative.ocelot.tasks;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Responsible for enabling the multiple threads to coordinate their actions.
 *
 * @author Robert Wapshott
 */
public class Coordinator {
    private Set<File> folders = new HashSet<File>();
    private boolean scanning = false;
    private boolean started = false;

    public synchronized void fileToBeProcssed(File file) {
        folders.add(file);
    }

    public synchronized void fileProcessed(File file) {
        folders.remove(file);
    }

    public synchronized boolean hasFilesToProcess() {
        return !folders.isEmpty();
    }

    public synchronized boolean isScanning() {
        return scanning;
    }

    public synchronized void setScanning(boolean scanning) {
        this.scanning = scanning;
    }

    public synchronized boolean hasTomcatStarted() {
        return this.started;
    }

    public synchronized void setTomcatStarted(boolean started) {
        this.started = started;
    }
}
