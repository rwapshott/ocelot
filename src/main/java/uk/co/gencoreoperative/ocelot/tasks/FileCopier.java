package uk.co.gencoreoperative.ocelot.tasks;

import com.google.inject.Inject;
import uk.co.gencoreoperative.fileutils.FileUtils;
import uk.co.gencoreoperative.ocelot.utils.Stats;

import java.io.File;

/**
 * Responsible for coping class files from the source folder to the target folder.
 *
 * @author Robert Wapshott
 */
public class FileCopier implements Runnable {
    private final Coordinator coordinator;
    private final Stats stats;

    private File source;
    private File target;

    @Inject
    public FileCopier(Coordinator coordinator, Stats stats) {
        this.coordinator = coordinator;
        this.stats = stats;
    }

    public void setSource(File source) {
        this.source = source;
    }

    public void setTarget(File target) {
        this.target = target;
    }

    @Override
    public void run() {
        if (!target.exists() || target.lastModified() < source.lastModified()) {
            stats.incrementFilesCopied();
            new FileUtils().copy(source, target);
        }
        coordinator.fileProcessed(target);
    }
}
