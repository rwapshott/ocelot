package uk.co.gencoreoperative.ocelot.wizard;

import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;

/**
 * [Work in Progress, not functioning and not currently planned.]
 *
 * Responsible for guiding the user through the process to configuring Ocelot
 * with the locations of their checkout, apache installation and filters.
 *
 * @author Robert Wapshott
 */
public class Setup {
    public OcelotConfig setup(OcelotConfig current) {
        OcelotConfig r = new OcelotConfig();

        File tomcatFolder = getTomcatFolder(current.getApacheTomcatHome());
        if (tomcatFolder == null) return null;
        r.setApacheTomcatHome(tomcatFolder);

        return r;
    }

    private File getTomcatFolder(File current) {
        String path;
        if (current == null) {
            path = "";
        } else {
            path = current.getPath();
        }

        String folder = query("Where is Tomcat installed?", current.getPath());
        if (folder == null) return null;
        File tomcatFolder = new File(folder);
        if (tomcatFolder.exists()) return tomcatFolder;
        Ocelot.log("Folder did not exist");
        return getTomcatFolder(current);
    }

    /**
     * Query the user by writing a question to the stdout and reading their response on the stdin.
     *
     * @param question A question to ask to the user.
     * @param current The default if present. Non null.
     * @return Null indicates the user has requested to quit. Otherwise should be taken as their response to the query.
     */
    public String query(String question, String current) {
        Ocelot.log(MessageFormat.format(
                "{0}\n" +
                        "Current: {1}\n" +
                        "Type 'quit' to abort.", question, current));
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String response;
        try {
            response = reader.readLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        if (response.equals("")) {

            if (current == null) {
                Ocelot.log("No default, please enter a value.");
            }


        }

        if ("quit".equals(response)) {
            return null;
        }

        if (response.equals("")) {
            return current;
        }

        return response;
    }
}
