package uk.co.gencoreoperative.ocelot;

/**
 * Responsible for storing the application global constants.
 *
 * @author robert.wapshott@forgerock.com
 */
public class OcelotConstants {
    public static final String APP_NAME = "ocelot";
    public static final String SHOW_MODE = "show";
    public static final String BOUNCE_MODE = "bounce";
    public static final String INIT_MODE = "init";
    public static final String RUN_MODE = "run";
    public static final String WAR_MODE = "war";
    public static final String HELP_MODE = "help";
    public static final String DEPLOY_MODE = "deploy";
    public static final String DEPLOY_THIS_MODE = "deployThis";
}
